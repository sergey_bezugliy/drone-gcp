FROM google/cloud-sdk:alpine
RUN set -ex \
  && apk --update --no-cache add bash bash-dbg dpkg-dev \
  && mkdir -p ~/.secrets \
  && mkdir -p ~/bin \
  && gcloud components install kubectl
COPY gcp-init /sbin/
RUN chmod a+x /sbin/gcp-init

# CMD ["gcp-init"]