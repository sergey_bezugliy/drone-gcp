# drone.io Google Cloud Platform plugin

Build state:
[![Build Status](https://ci.codenv.top/api/badges/sergey_bezugliy/drone-gcp/status.svg)](https://ci.codenv.top/sergey_bezugliy/drone-gcp)

Plugin based on official google/cloud-sdk:alpine image.
[Image at Docker Hub](https://cloud.docker.com/repository/docker/sbezugliy/drone-gcp)

## Usage

To be done...

### Credential configuration

### .drone.yml configuration

### Installed Google Cloud SDK Modules

### Installing and activating additional modules

## Author

Sergey Bezugliy

s.bezugliy@gmail.com

s.bezugliy@codenv.top
